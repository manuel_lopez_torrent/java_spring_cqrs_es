package com.malotor.cqrs.domain.model;

public class OrderedMenuItemCouldNotBeServed extends Exception {

    public OrderedMenuItemCouldNotBeServed(String message) {
        super(message);
    }
}
