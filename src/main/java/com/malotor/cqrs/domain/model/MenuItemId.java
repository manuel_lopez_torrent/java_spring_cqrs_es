package com.malotor.cqrs.domain.model;


public class MenuItemId<T> {

    private T value;

    public MenuItemId(T value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        return this.value.equals( ((MenuItemId<T>) obj).id() );
    }


    public T id() {
        return this.value;
    }

    @Override
    public String toString() {
        return (String) this.value.toString();
    }

    public static MenuItemId<Integer> createFromInt(Integer value)
    {
        return new MenuItemId<Integer>(value);
    }

    public static MenuItemId createFromString(String value)
    {
        return new MenuItemId<String>(value);
    }


}
