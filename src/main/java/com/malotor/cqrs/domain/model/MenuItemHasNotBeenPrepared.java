package com.malotor.cqrs.domain.model;

public class MenuItemHasNotBeenPrepared extends OrderedMenuItemCouldNotBeServed {
    public MenuItemHasNotBeenPrepared() {
        super("Item has not been prepared");
    }
}
