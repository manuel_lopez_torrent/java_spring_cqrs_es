package com.malotor.cqrs.domain.model;

public class MustPayEnoughException extends Exception {
    public MustPayEnoughException() {
        super("Amount is not enough");
    }
}
