package com.malotor.cqrs.domain.model;

public class MenuItemAlreadyHasBeenOrdered extends Exception {
    public MenuItemAlreadyHasBeenOrdered() {
        super("Item has been already ordered");
    }
}

