package com.malotor.cqrs.domain.model;

public class OrderedMenuItemIsNotPreparable extends Exception {

    public OrderedMenuItemIsNotPreparable() {
        super("Ordered menu item is not preparable");
    }
}
