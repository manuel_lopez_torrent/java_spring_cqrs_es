package com.malotor.cqrs.domain.model;

public class Food extends OrderedMenuItem  {
    public Food(Integer id, Double price) {
        super(id, price);
    }

    public boolean isOutStanding() {
        return this.state == State.OUTSTANDING;
    }
    public boolean isPrepared() {
        return this.state == State.PREPARED;
    }
    public boolean isServed() {
        return this.state == State.SERVED;
    }

    public void prepare() {
        state = State.PREPARED;
    }


    public void serve() throws MenuItemHasNotBeenPrepared {
        if (!isPrepared())
            throw new MenuItemHasNotBeenPrepared();
        state = State.SERVED;
    }

    public boolean mustBePrepared()
    {
        return true;
    }
}
