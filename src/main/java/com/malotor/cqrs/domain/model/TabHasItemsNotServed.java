package com.malotor.cqrs.domain.model;

public class TabHasItemsNotServed extends Exception {
    public TabHasItemsNotServed() {
        super("Tab has items not served");
    }
}
