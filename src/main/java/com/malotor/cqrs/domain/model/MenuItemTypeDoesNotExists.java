package com.malotor.cqrs.domain.model;

public class MenuItemTypeDoesNotExists extends Exception {
    public MenuItemTypeDoesNotExists() {
        super("The menu item type does not exists");
    }
}
