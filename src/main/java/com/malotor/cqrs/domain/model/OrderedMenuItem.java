package com.malotor.cqrs.domain.model;

public abstract class OrderedMenuItem {
    public MenuItemId<Integer> id;
    private Double price;
    protected State state = State.OUTSTANDING;

    public OrderedMenuItem(Integer id, Double price) {
        this.id = MenuItemId.createFromInt(id);
        this.price = price;
    }

    public Integer getId() {
        return id.id();
    }

    public Double getPrice() {
        return price;
    }

    public State getState() {
        return state;
    }

    public static OrderedMenuItem create(String[] item) throws MenuItemTypeDoesNotExists {
        switch (item[0]) {
            case "drink":
                return new Drink(Integer.parseInt(item[1]), Double.parseDouble(item[2]));
            case "food":
                return new Food(Integer.parseInt(item[1]), Double.parseDouble(item[2]));
            default:
                throw new MenuItemTypeDoesNotExists();
        }

    }

    @Override
    public boolean equals(Object obj) {
        return this.id.equals(((OrderedMenuItem) obj).id);
    }

    abstract public boolean mustBePrepared();

    abstract public void serve() throws  MenuItemDoesNotExists, OrderedMenuItemCouldNotBeServed;
}
