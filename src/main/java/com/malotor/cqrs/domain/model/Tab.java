package com.malotor.cqrs.domain.model;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Tab {

    private String waiter;
    private Integer tableId;
    private Boolean opened;
    private Double tip = 0.0;
    private Double amount = 0.0;

    private List<OrderedMenuItem> orderedItems = new ArrayList<>();


    public Tab( Integer tableId, String waiter) {
        this.waiter = waiter;
        this.tableId = tableId;
        opened = true;
    }

    public static Tab create(Integer tableId, String waiter) {
        return new Tab(tableId, waiter);
    }

    public String getWaiter() {
        return waiter;
    }

    public Integer getTableId() {
        return tableId;
    }



    public OrderedMenuItem order(String[] item)  throws MenuItemAlreadyHasBeenOrdered, MenuItemTypeDoesNotExists
    {
        OrderedMenuItem orderedItem = OrderedMenuItem.create(item);
        assertItemsIsNotOrdered(orderedItem);
        amount += orderedItem.getPrice();
        orderedItems.add(orderedItem);
        return orderedItem;
    }



    public void serve(Integer id) throws MenuItemDoesNotExists, OrderedMenuItemCouldNotBeServed
    {

        OrderedMenuItem orderedMenuItem = getItem(id);

        orderedMenuItem.serve();

    }



    public void prepare(Integer id) throws MenuItemDoesNotExists, OrderedMenuItemIsNotPreparable
    {
        OrderedMenuItem orderedMenuItem = getItem(id);

        assertItemMustBePrepared(orderedMenuItem);

        ((Food) orderedMenuItem).prepare();
    }


    public void close(Double amountPayed) throws TabHasItemsNotServed, MustPayEnoughException
    {
        assertTabHasNotOutstandingItems();

        assertTabHasNotPreparedItems();

        assertAmountIsEnought(amountPayed);

        opened = false;

        tip = amountPayed - amount;
    }


    public Double getTip() {
        return tip;
    }


    public List<OrderedMenuItem> getOutstandingItems() {
        return orderedItems.stream().filter( item -> item.getState() == State.OUTSTANDING ).collect(Collectors.toList());
    }

    public List<OrderedMenuItem> getServedItems() {
        return orderedItems.stream().filter(item -> item.getState() == State.SERVED).collect(Collectors.toList());
    }

    public List<OrderedMenuItem> getPreparedItems() {
        return orderedItems.stream().filter(item -> item.getState() == State.PREPARED).collect(Collectors.toList());
    }


    public List<OrderedMenuItem> getOutstandingDrinks()
    {
        return getOutstandingItems();
    }

    public List<OrderedMenuItem> getOutstandingFood()
    {
        return getOutstandingItems();
    }


    public Boolean isOpen()
    {
        return opened;
    }

    private void assertItemMustBePrepared(OrderedMenuItem item) throws OrderedMenuItemIsNotPreparable
    {
        if (!item.mustBePrepared())
            throw new OrderedMenuItemIsNotPreparable();
    }

    private void assertItemsIsNotOrdered(OrderedMenuItem item) throws MenuItemAlreadyHasBeenOrdered
    {
        if (orderedItems.contains(item))
            throw new MenuItemAlreadyHasBeenOrdered();
    }


    private void assertItemHasBeenOrdered(OrderedMenuItem orderedMenuItem) throws MenuItemDoesNotExists
    {
        if (orderedMenuItem == null ) throw new MenuItemDoesNotExists();
    }

    private void assertTabHasNotOutstandingItems() throws TabHasItemsNotServed
    {
        if (!getOutstandingItems().isEmpty())
            throw new TabHasItemsNotServed();
    }

    private void assertTabHasNotPreparedItems() throws TabHasItemsNotServed
    {
        if (!getPreparedItems().isEmpty())
            throw new TabHasItemsNotServed();
    }

    private void assertAmountIsEnought(Double amountPayed) throws MustPayEnoughException
    {
        if (amount > amountPayed)
            throw new MustPayEnoughException();
    }


    private OrderedMenuItem getItem(Integer id) throws  MenuItemDoesNotExists
    {
        OrderedMenuItem orderedMenuItem = orderedItems.stream().filter(menuItem -> menuItem.getId().equals(id)).findFirst().orElse(null);

        assertItemHasBeenOrdered(orderedMenuItem);

        return orderedMenuItem;
    }


}
