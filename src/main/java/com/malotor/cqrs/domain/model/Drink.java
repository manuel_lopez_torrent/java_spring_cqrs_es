package com.malotor.cqrs.domain.model;

public class Drink extends OrderedMenuItem  {
    public Drink(Integer id, Double price) {
        super(id, price);
    }

    public boolean isOutStanding() {
        return this.state == State.OUTSTANDING;
    }
    public boolean isServed() {
        return this.state == State.SERVED;
    }

    public void serve()  {
        state = State.SERVED;
    }

    public boolean mustBePrepared()
    {
        return false;
    }
}
