package com.malotor.cqrs.domain.model;

public class MenuItemDoesNotExists extends Exception {
    public MenuItemDoesNotExists() {
        super("The menu item has not been ordered");
    }
}
