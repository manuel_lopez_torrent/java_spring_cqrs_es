package com.malotor.cqrs.domain.model;

public enum State {
    OUTSTANDING,
    PREPARED,
    SERVED
}
