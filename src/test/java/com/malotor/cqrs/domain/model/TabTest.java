package com.malotor.cqrs.domain.model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.rules.ExpectedException;

public class TabTest {

    private Tab tab;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void initialize() {
        String waiter = "John Doe";
        Integer tableId = 1;

        tab = TabMotherObject.create(tableId, waiter);
    }

    @Test
    public void testShouldBeCreatedWithAWaiterAndTable() {
        assertEquals((Integer) 1, tab.getTableId());
        assertEquals( "John Doe", tab.getWaiter());
    }

    @Test
    public void testShouldOpenned() {
        assertEquals(true, tab.isOpen());
    }


    @Test
    public void testShouldNotHaveOutstandingMenuItemsWhenIsJustCreated() {
        assertEquals(0,tab.getOutstandingItems().size());
    }

    @Test
    public void testShouldNotHavePreparedMenuItemsWhenIsJustCreated() {
        assertEquals(0,tab.getPreparedItems().size());
    }

    @Test
    public void testShouldNotHaveServeddMenuItemsWhenIsJustCreated() {
        assertEquals(0,tab.getServedItems().size());
    }

    @Test
    public void testShouldPlaceNewOrderedDrinksAsOutstanding() throws Exception {

        Drink drink = (Drink) tab.order(MenuItemFactory.createDrink(1));
        assertEquals(1,tab.getOutstandingDrinks().size());
        assertTrue(assertMenuItemIsInList(drink,tab.getOutstandingDrinks()));
        assertTrue(drink.isOutStanding());

        Drink otherDrink = (Drink) tab.order(MenuItemFactory.createDrink(2));
        assertEquals(2,tab.getOutstandingDrinks().size());
        assertTrue(assertMenuItemIsInList(otherDrink,tab.getOutstandingDrinks()));
        assertTrue(otherDrink.isOutStanding());
    }

    @Test
    public void testShouldPlaceNewOrderedFoodAsOutstanding() throws Exception {

        tab.order(MenuItemFactory.createFood(3));
        assertEquals(1,tab.getOutstandingFood().size());
        assertTrue(assertMenuItemIsInList(3,tab.getOutstandingFood()));

        tab.order(MenuItemFactory.createFood(4));
        assertEquals(2,tab.getOutstandingFood().size());
        assertTrue(assertMenuItemIsInList(4,tab.getOutstandingFood()));
    }


    @Test(expected = MenuItemAlreadyHasBeenOrdered.class)
    public void testShouldFailToOrderADrinkIfItHasBeenAlreadyOrdered() throws Exception {

        tab.order(MenuItemFactory.createDrink(5));
        tab.order(MenuItemFactory.createDrink(5));

    }

    @Test(expected = MenuItemAlreadyHasBeenOrdered.class)
    public void testShouldFailToOrderAFoodIfItHasBeenAlreadyOrdered() throws Exception {

        tab.order(MenuItemFactory.createFood(4));
        tab.order(MenuItemFactory.createFood(4));
    }



    @Test(expected = MenuItemDoesNotExists.class)
    public void testShouldFailToPlaceADrinkServedIfItsHasNotBeenOrdered() throws  Exception
    {
        tab.serve(1);
    }


    @Test
    public void testShouldAllowPlaceDrinksServedIfItsOutstanding() throws Exception {

        Drink drink = (Drink) tab.order(MenuItemFactory.createDrink(1));
        tab.serve(1);

        assertEquals(0,tab.getOutstandingDrinks().size());
        assertEquals(1,tab.getServedItems().size());
        assertTrue(assertMenuItemIsInList(1,tab.getServedItems()));
        assertFalse(assertMenuItemIsInList(1,tab.getOutstandingDrinks()));
        assertTrue(drink.isServed());
    }

    @Test(expected = MenuItemDoesNotExists.class)
    public void testShouldFailToPlaceFoodPreparedIfItHaveNotBeenOrdered() throws  Exception
    {
        tab.prepare(1);
    }

    @Test
    public void testShouldPrepareFoodBefore() throws  Exception {

        Food food = (Food) tab.order(MenuItemFactory.createFood(1));
        tab.prepare(1);

        assertEquals(0,tab.getOutstandingFood().size());
        assertEquals(1,tab.getPreparedItems().size());
        assertTrue(assertMenuItemIsInList(1,tab.getPreparedItems()));
        assertFalse(assertMenuItemIsInList(1,tab.getOutstandingFood()));

        assertFalse(food.isOutStanding());
        assertTrue(food.isPrepared());
    }

    @Test(expected = MenuItemHasNotBeenPrepared.class)
    public void testShouldPrepareFoodBeforeServeIt() throws  Exception {

        tab.order(MenuItemFactory.createFood(1));
        tab.serve(1);
    }

    @Test
    public void testShouldServePreparedFood() throws  Exception
    {

        Food food = (Food) tab.order(MenuItemFactory.createFood(1));
        tab.prepare(1);
        tab.serve(1);

        assertEquals(0,tab.getPreparedItems().size());
        assertEquals(1,tab.getServedItems().size());
        assertTrue(assertMenuItemIsInList(1,tab.getServedItems()));
        assertFalse(assertMenuItemIsInList(1,tab.getPreparedItems()));

        assertFalse(food.isOutStanding());
        assertFalse(food.isPrepared());
        assertTrue(food.isServed());
    }


    @Test(expected = TabHasItemsNotServed.class)
    public void testShouldNotBeCloseIfAlHaveOutstandingItems() throws  Exception {

        tab.order(MenuItemFactory.createDrink(5));
        tab.close(1.0);
    }

    @Test(expected = TabHasItemsNotServed.class)
    public void testShouldNotBeCloseIfhasPreparedItems() throws Exception {

        tab.order(MenuItemFactory.createFood(5));
        tab.prepare(5);
        tab.close(1.0);
    }

    @Test(expected = MustPayEnoughException.class)
    public void testShouldNotBeCloseIfAmountIsNotEnought() throws  Exception{

        tab.order(MenuItemFactory.createFood(5, 10));
        tab.prepare(5);
        tab.serve(5);
        tab.close(1.0);

    }

    @Test
    public void testShouldCloseIfAmountIsEnought() throws  Exception {

        tab.order(MenuItemFactory.createDrink(1, 5));
        tab.order(MenuItemFactory.createFood(5, 10));
        tab.serve(1);
        tab.prepare(5);
        tab.serve(5);
        tab.close(15.0);
        assertEquals(false, tab.isOpen());
    }

    @Test
    public void testShouldHasTipsIfAmountIsHigherThanTabPrice() throws  Exception {

        tab.order(MenuItemFactory.createDrink(1, 2));
        tab.order(MenuItemFactory.createFood(5, 10));
        tab.serve(1);
        tab.prepare(5);
        tab.serve(5);
        tab.close(15.0);
        assertEquals(false, tab.isOpen());
        assertEquals((Double) 3.0, tab.getTip());
    }


    private boolean assertMenuItemIsInList(Integer id, List<OrderedMenuItem> orderedMenuItemsList)
    {

        if (orderedMenuItemsList.stream()
                .filter(x -> x.getId().equals(id))
                .findAny()
                .orElse(null) == null) return false;

        return true;
    }

    private boolean assertMenuItemIsInList(OrderedMenuItem item, List<OrderedMenuItem> orderedMenuItemsList)
    {
        return orderedMenuItemsList.contains(item);
    }
}

class TabMotherObject {

    public static Tab create(int table , String waiter)
    {
        return new Tab(table, waiter);
    }
}



class MenuItemFactory {

    public static String[] createDrink(int id )
    {
        return createDrink(id,1.0);
    }

    public static String[] createDrink(int id , double price)
    {
        return createItem("drink",id,price);
    }


    public static String[] createFood(int id )
    {
        return createFood(id,1.0);

    }

    public static String[] createFood(int id, double price)
    {
        return createItem("food",id,price);
    }

    public static String[] createItem(String type, int id, double price)
    {
        return new String[] { type, Integer.toString(id), Double.toString(price) };
    }
}