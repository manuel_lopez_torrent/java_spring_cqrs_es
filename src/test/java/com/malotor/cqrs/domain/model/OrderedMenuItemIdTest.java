package com.malotor.cqrs.domain.model;

import org.junit.Test;

import static org.junit.Assert.*;


public class OrderedMenuItemIdTest {

    @Test
    public void shouldBeEqualThanOtherIfHaveSameValues()
    {
        assertEquals(MenuItemIdBuilder.createFromInt(4), MenuItemIdBuilder.createFromInt(4));
    }


    @Test
    public void shouldNoBeEqualThanOtherIfHaveDiferentValues()
    {
        assertNotEquals(MenuItemIdBuilder.createFromInt(4), MenuItemIdBuilder.createFromInt(5));
    }

    @Test
    public void shouldBeCreatedFromString()
    {
        assertEquals(MenuItemIdBuilder.createFromString("anId"), MenuItemIdBuilder.createFromString("anId"));
    }

    @Test
    public void shouldCastedToString()
    {
        assertEquals("4", MenuItemIdBuilder.createFromInt(4).toString());
        assertEquals("anId", MenuItemIdBuilder.createFromString("anId").toString());

    }
}


class MenuItemIdBuilder
{
    public static MenuItemId createFromInt(Integer value)
    {
        return MenuItemId.createFromInt(value);
    }

    public static MenuItemId createFromString(String value)
    {
        return MenuItemId.createFromString(value);
    }
}